package pk.labs.LabA;

import pk.labs.LabA.ControlPanel.ControlPanelBean;
import pk.labs.LabA.Display.DisplayBean;
import pk.labs.LabA.Main.WindaBean;
import pk.labs.LabA.Contracts.*;

public class LabDescriptor {

    // region P1
    public static String displayImplClassName = DisplayBean.class.getName();
    public static String controlPanelImplClassName = ControlPanelBean.class.getName();

    public static String mainComponentSpecClassName = Winda.class.getName();
    public static String mainComponentImplClassName = WindaBean.class.getName();
    // endregion

    // region P2
    public static String mainComponentBeanName = "main";
    public static String mainFrameBeanName = "app";
    // endregion

    // region P3
    public static String sillyFrameBeanName = "silly";
    // endregion
}